package view;

import java.util.Iterator;
import java.util.List;

import oracle.adf.view.rich.component.rich.data.RichTreeTable;

import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.domain.Number;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;

import org.apache.myfaces.trinidad.event.RowDisclosureEvent;
import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.RowKeySet;

public class TestBean {
    private RichTreeTable tree;
    private RichPanelGroupLayout pgl;

    public TestBean() {
        super();
    }

    public void setTree(RichTreeTable tree) {
        this.tree = tree;
    }

    public RichTreeTable getTree() {
        return tree;
    }

    public void rowDisclosureListener(RowDisclosureEvent rowDisclosureEvent) {
        RowKeySet addedSet = rowDisclosureEvent.getAddedSet();
        Number deptId = null;
        Row currentRow = null;
        Iterator rksIterator = addedSet.iterator();
        if (rksIterator.hasNext()) {
            List key = (List)rksIterator.next();
            JUCtrlHierBinding treeTableBinding = null;
            treeTableBinding =
                    (JUCtrlHierBinding)((CollectionModel)getTree().getValue()).getWrappedData();
            currentRow =
                    treeTableBinding.getRowIterator().getRow((Key)key.get(0));
            if (currentRow != null) {
                deptId = (Number)currentRow.getAttribute("DepartmentId");
            }
        }
        if (deptId != null) {
            RowKeySet rks = getTree().getDisclosedRowKeys();
            for (Iterator itr = rks.iterator(); itr.hasNext(); ) {
                List key = (List)itr.next();
                JUCtrlHierBinding treeTableBinding = null;
                treeTableBinding =
                        (JUCtrlHierBinding)((CollectionModel)getTree().getValue()).getWrappedData();
                Row row =
                    treeTableBinding.getRowIterator().getRow((Key)key.get(0));
                if (row != null) {
                    if (deptId.equals(row.getAttribute("DepartmentId"))) {
                        currentRow.setAttribute("IsExpanded", true);

                    }
                }
            }
        }
        if(pgl!=null){
            AdfFacesContext.getCurrentInstance().addPartialTarget(pgl);
        }
        rowDisclosureEvent.getAddedSet().clear();
    }

    public void setPgl(RichPanelGroupLayout pgl) {
        this.pgl = pgl;
    }

    public RichPanelGroupLayout getPgl() {
        return pgl;
    }
}
