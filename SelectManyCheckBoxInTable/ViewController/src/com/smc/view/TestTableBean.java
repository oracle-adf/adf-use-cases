package com.smc.view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.util.Map;

import javax.el.ELContext;
import javax.el.ExpressionFactory;
import javax.el.ValueExpression;

import javax.faces.application.Application;
import javax.faces.context.FacesContext;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTable;

import oracle.adf.view.rich.context.AdfFacesContext;

import org.apache.myfaces.trinidad.model.CollectionModel;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.RowIterator;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Number;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import org.apache.myfaces.trinidad.model.RowKeySet;

public class TestTableBean {

    public TestTableBean() {
        super();
    }
    private BindingContainer bindings;
    private List selectedRoles;
    private RichTable smcTable;

    public void setBindings(BindingContainer bindings) {
        this.bindings = bindings;
    }

    public BindingContainer getBindings() {
        return bindings;
    }

    public void setSelectedRoles(List selectedRoles) {
        this.selectedRoles = selectedRoles;
    }

    public void setSmcTable(RichTable smcTable) {
        this.smcTable = smcTable;
    }

    public RichTable getSmcTable() {
        return smcTable;
    }

    public static Object resolveExpression(String expression) {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        Application app = facesContext.getApplication();
        ExpressionFactory elFactory = app.getExpressionFactory();
        ELContext elContext = facesContext.getELContext();
        ValueExpression valueExp =
            elFactory.createValueExpression(elContext, expression,
                                            Object.class);
        return valueExp.getValue(elContext);
    }

    public List getSelectedRoles() {
        JUCtrlHierNodeBinding row =
            (JUCtrlHierNodeBinding)resolveExpression("#{row}");
        String storedRoles = (String)row.getAttribute("StoredRoles");
        if (storedRoles != null) {
            String roles[] = storedRoles.split(";");
            List role = new ArrayList();
            int size = roles.length;
            if (size > 0) {
                for (int i = 0; i < size; i++) {
                    try {
                        role.add(new Number(roles[i]));
                    } catch (Exception e) {
                    }
                }
            }
            if (role.size() > 0)
                return role;
        }
        return null;
    }

    public Row getSelectedRow() {
        RichTable _table = getSmcTable();
        CollectionModel _tableModel = (CollectionModel)_table.getValue();
        Object _selectedRowData = _table.getSelectedRowData();
        JUCtrlHierNodeBinding _nodeBinding =
            (JUCtrlHierNodeBinding)_selectedRowData;
        RowKeySet rks = _table.getSelectedRowKeys();
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        List keySet =
            (List)adfFacesContext.getPageFlowScope().get("SelectedKeySet");
        if (keySet == null) {
            keySet = new ArrayList();
        }
        for (Iterator itr = rks.iterator(); itr.hasNext(); ) {
            Key key = (Key)((List)itr.next()).get(0);
            if (!keySet.contains(key))
                keySet.add(key);
        }
        adfFacesContext.getPageFlowScope().put("SelectedKeySet", keySet);
        Row row = _nodeBinding.getRow();
        return row;
    }

    public void onRoleChange(ValueChangeEvent valueChangeEvent) {
        List<Number> selectedListFromUI = null;
        selectedListFromUI = (ArrayList<Number>)valueChangeEvent.getNewValue();
        Row row = getSelectedRow();
        StringBuffer attr = new StringBuffer();
        if (selectedListFromUI != null) {
            Iterator itr = selectedListFromUI.iterator();
            while (itr.hasNext()) {
                attr.append(itr.next()).append(";");
            }
        }
        if (attr.length() > 0)
            row.setAttribute("SelectedRoles",
                             attr.substring(0, attr.length() - 1));
        else
            row.setAttribute("SelectedRoles", "");
    }

    public String onCommit() {
        Map pageFlow = 
            (Map)AdfFacesContext.getCurrentInstance().getPageFlowScope();
        List keySet = (List)pageFlow.get("SelectedKeySet");
        if (keySet == null || keySet.size() == 0)
            return null;
        boolean isDirty = false;
        RichTable _table = getSmcTable();
        CollectionModel _tableModel = (CollectionModel)_table.getValue();
        JUCtrlHierBinding _adfTableBinding =
            (JUCtrlHierBinding)_tableModel.getWrappedData();
        DCIteratorBinding _tableIteratorBinding =
            _adfTableBinding.getDCIteratorBinding();
        RowSetIterator empRSIter = _tableIteratorBinding.getRowSetIterator();
        DCBindingContainer bc = (DCBindingContainer)getBindings();
        for (Iterator itr = keySet.iterator(); itr.hasNext(); ) {
            Key key = (Key)itr.next();
            Row currentRow = empRSIter.getRow(key);
            if (currentRow != null &&
                currentRow.getAttribute("SelectedRoles") != null) {
                RowIterator iter =
                    (RowIterator)currentRow.getAttribute("EmpRolesVO");
                isDirty = true;
                for (Row row : iter.getAllRowsInRange()) {
                    row.remove();
                }
                String roles =
                    (String)currentRow.getAttribute("SelectedRoles");
                String role[] = roles.split(";");
                int size = role.length;
                if (size > 0 && !"".equals(role[0])) {
                    for (int i = 0; i < size; i++) {
                        Row row = iter.createRow();
                        row.setAttribute("EmpNo",
                                         currentRow.getAttribute("EmpId"));
                        row.setAttribute("RoleId", role[i]);
                        iter.insertRow(row);
                    }
                }
            }
        }
        if (isDirty) {
            OperationBinding op = bc.getOperationBinding("Commit");
            if (op != null) {
                op.execute();
            }
        }
        AdfFacesContext.getCurrentInstance().getPageFlowScope().clear();
        return null;
    }
}
