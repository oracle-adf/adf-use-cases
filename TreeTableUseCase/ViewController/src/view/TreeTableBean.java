package view;

import java.util.ArrayList;
import java.util.List;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTreeTable;

import oracle.adf.view.rich.component.rich.input.RichSelectBooleanRadio;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;

import oracle.jbo.Key;
import oracle.jbo.Row;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;
import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;
import oracle.jbo.uicli.binding.JUIteratorBinding;

import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.ModelUtils;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.model.RowKeySetImpl;
import org.apache.myfaces.trinidad.model.RowKeySetTreeImpl;

public class TreeTableBean {
    private RichTreeTable treeTable;
    private BindingContainer bindings;
    RowKeySet _disclosedRowKeys = new RowKeySetTreeImpl();

    public TreeTableBean() {
        super();
    }
    
    public void setBindings(BindingContainer bindings) {
        this.bindings = bindings;
    }

    public BindingContainer getBindings() {
        return bindings;
    }

    public void setTreeTable(RichTreeTable treeTable) {
        this.treeTable = treeTable;
    }

    public RichTreeTable getTreeTable() {
        return treeTable;
    }
    
    public List<Key> getListKey(Object obj) {
        Object[] objArr = new Object[] { obj };
        List li = new ArrayList();
        li.add(new Key(objArr));
        return li;
    }

    public void onSelectDepartment(ValueChangeEvent valueChangeEvent) {
        Object newVal = valueChangeEvent.getNewValue();
        Object oldVal = valueChangeEvent.getOldValue();

        RichSelectBooleanRadio uiComp = (RichSelectBooleanRadio)valueChangeEvent.getSource();
        Object obj = uiComp.getAttributes().get("selectedDeptId");
        if((Boolean)newVal){
            _disclosedRowKeys.add(getListKey(obj));
        }
//
//        RichTreeTable _treeTable = getTreeTable();
//        CollectionModel _treeTableModel = (CollectionModel)_treeTable.getValue();
//        JUCtrlHierBinding _adfTreeTableBinding = (JUCtrlHierBinding)_treeTableModel.getWrappedData();
//
//        JUCtrlHierNodeBinding nodeBinding = null;
//        nodeBinding = _adfTreeTableBinding.findNodeByKeyPath(getListKey(obj));
//        JUIteratorBinding iterator = nodeBinding.getIteratorBinding();
//        String keyStr = nodeBinding.getRowKey().toStringFormat(true);
//        iterator.setCurrentRowWithKey(keyStr);
//        if ((Boolean)newVal) {
//            RowKeySet selectedRkSet = new RowKeySetImpl();
//            selectedRkSet.add(getListKey(obj));
//            _treeTable.setSelectedRowKeys(selectedRkSet);
//            _treeTable.setDisclosedRowKeys(selectedRkSet);
//            DCBindingContainer bc = (DCBindingContainer)getBindings();
//            DCIteratorBinding iter = bc.findIteratorBinding("EmployeesIterator");
//            for (Row row : iter.getAllRowsInRange()) {
//                row.setAttribute("SelectEmp", true);
//            }
//        } else {
//            DCBindingContainer bc = (DCBindingContainer)getBindings();
//            DCIteratorBinding iter = bc.findIteratorBinding("EmployeesIterator");
//            for (Row row : iter.getAllRowsInRange()) {
//                row.setAttribute("SelectEmp", false);
//            }
//        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getTreeTable());
    }

    public void setDisclosedRowKeys(RowKeySet _disclosedRowKeys) {
        this._disclosedRowKeys = _disclosedRowKeys;
    }

    public RowKeySet getDisclosedRowKeys() {
        return _disclosedRowKeys;
    }
}
