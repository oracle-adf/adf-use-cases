package com.test.view;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.binding.BindingContainer;

import oracle.jbo.Row;

public class TestBean {
    public TestBean() {
        super();
    }
    private List list;
    private BindingContainer bindings;
    private static final ArrayList daysOfWeek =
        new ArrayList<String>(Arrays.asList("Mon", "Tue", "Wed", "Thu", "Fri",
                                            "Sat", "Sun"));

    public void setList(List list) {
        this.list = list;
    }

    public List getList() {
        List selectedList = new ArrayList();
        DCBindingContainer bc = (DCBindingContainer)getBindings();
        DCIteratorBinding ir =
            (DCIteratorBinding)bc.findIteratorBinding("DayWeekVO1Iterator");
        Row row = ir.getCurrentRow();
        if (row != null) {
            Iterator itr = daysOfWeek.iterator();
            while (itr.hasNext()) {
                String day = (String)itr.next();
                if ("Y".equalsIgnoreCase((String)row.getAttribute(day)))
                    selectedList.add(day);
            }
        }
        return selectedList;
    }

    public void setBindings(BindingContainer bindings) {
        this.bindings = bindings;
    }

    public BindingContainer getBindings() {
        return bindings;
    }

    public void onValueChange(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        ArrayList selectedList = null;
        if (valueChangeEvent.getOldValue().equals(valueChangeEvent.getNewValue()))
            return;
        selectedList = (ArrayList)valueChangeEvent.getNewValue();
        DCBindingContainer bc = (DCBindingContainer)getBindings();
        DCIteratorBinding ir =
            (DCIteratorBinding)bc.findIteratorBinding("DayWeekVO1Iterator");
        Row row = ir.getCurrentRow();
        List unselectedDays = new ArrayList();
        unselectedDays.addAll(daysOfWeek);
        if (selectedList != null) {
            Iterator itr = selectedList.iterator();
            while (itr.hasNext()) {
                row.setAttribute(itr.next().toString(), "Y");
            }
            unselectedDays.removeAll(selectedList);
        }
        Iterator itr = unselectedDays.iterator();
        while (itr.hasNext()) {
            row.setAttribute((String)itr.next(), "N");
        }
    }
}
