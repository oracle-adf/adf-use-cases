package com.table.view;

import com.table.view.common.CustomPagePhaseListener;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.data.RichTreeTable;
import oracle.adf.view.rich.component.rich.input.RichSelectBooleanRadio;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;

import oracle.jbo.Row;
import oracle.jbo.RowSet;
import oracle.jbo.RowSetIterator;
import oracle.jbo.domain.Number;
import oracle.jbo.uicli.binding.JUCtrlHierBinding;

import oracle.jbo.uicli.binding.JUCtrlHierNodeBinding;

import oracle.jbo.uicli.binding.JUIteratorBinding;

import org.apache.myfaces.trinidad.model.CollectionModel;
import org.apache.myfaces.trinidad.model.ModelUtils;
import org.apache.myfaces.trinidad.model.RowKeySet;
import org.apache.myfaces.trinidad.model.RowKeySetImpl;
import org.apache.myfaces.trinidad.model.RowKeySetTreeImpl;

public class TreeTableBean extends CustomPagePhaseListener {
    private RichTreeTable treeTable;
    private BindingContainer bindings;

    public TreeTableBean() {
        super();
    }

    public void setBindings(BindingContainer bindings) {
        this.bindings = bindings;
    }

    public BindingContainer getBindings() {
        return bindings;
    }

    public void setTreeTable(RichTreeTable treeTable) {
        this.treeTable = treeTable;
    }

    public RichTreeTable getTreeTable() {
        return treeTable;
    }

    public String onExpandAll() {
        RichTreeTable tree = getTreeTable();
        RowKeySet _disclosedRowKeys = new RowKeySetTreeImpl(true);
        _disclosedRowKeys.setCollectionModel(ModelUtils.toTreeModel(tree.getValue()));
        tree.setDisclosedRowKeys(_disclosedRowKeys);
        return null;
    }

    public String onDisableAll() {
        RichTreeTable tree = getTreeTable();
        RowKeySet _disclosedRowKeys = tree.getDisclosedRowKeys();
        if (_disclosedRowKeys != null && _disclosedRowKeys.size() > 0) {
            _disclosedRowKeys.clear();
        }
        tree.setDisclosedRowKeys(_disclosedRowKeys);
        return null;
    }

    public List<Key> getListKey(Object obj) {
        Object[] objArr = new Object[] { obj };
        List<Key> li = new ArrayList<Key>();
        li.add(new Key(objArr));
        return li;
    }

    public void onSelectDepartment(ValueChangeEvent valueChangeEvent) {
        Object newVal = valueChangeEvent.getNewValue();
        Object oldVal = valueChangeEvent.getOldValue();

        RichSelectBooleanRadio uiComp =
            (RichSelectBooleanRadio)valueChangeEvent.getSource();
        Object obj = uiComp.getAttributes().get("selectedDeptId");
        AdfFacesContext.getCurrentInstance().getPageFlowScope().put("DepartmentId",
                                                                    obj);

        RichTreeTable _treeTable = getTreeTable();
        CollectionModel _treeTableModel =
            (CollectionModel)_treeTable.getValue();
        JUCtrlHierBinding _adfTreeTableBinding =
            (JUCtrlHierBinding)_treeTableModel.getWrappedData();

        JUCtrlHierNodeBinding nodeBinding = null;
        nodeBinding = _adfTreeTableBinding.findNodeByKeyPath(getListKey(obj));
        JUIteratorBinding iterator = nodeBinding.getIteratorBinding();
        String keyStr = nodeBinding.getRowKey().toStringFormat(true);
        iterator.setCurrentRowWithKey(keyStr);
        if ((Boolean)newVal) {
            RowKeySet selectedRkSet = new RowKeySetImpl();
            selectedRkSet.add(getListKey(obj));
            _treeTable.setSelectedRowKeys(selectedRkSet);
            _treeTable.setDisclosedRowKeys(selectedRkSet);
            DCBindingContainer bc = (DCBindingContainer)getBindings();
            DCIteratorBinding iter =
                bc.findIteratorBinding("EmployeesIterator");
            for (Row row : iter.getAllRowsInRange()) {
                row.setAttribute("SelectEmp", true);
            }
        } else {
            DCBindingContainer bc = (DCBindingContainer)getBindings();
            DCIteratorBinding iter =
                bc.findIteratorBinding("EmployeesIterator");
            for (Row row : iter.getAllRowsInRange()) {
                row.setAttribute("SelectEmp", false);
            }
        }
        AdfFacesContext.getCurrentInstance().addPartialTarget(getTreeTable());
    }

    @Override
    public void onPageLoad() {
//        if (!AdfFacesContext.getCurrentInstance().isPostback()) {
//            RichTreeTable _treeTable = getTreeTable();
//            CollectionModel _treeTableModel =
//                (CollectionModel)_treeTable.getValue();
//            JUCtrlHierBinding _adfTreeTableBinding =
//                (JUCtrlHierBinding)_treeTableModel.getWrappedData();
//
//            Object obj = AdfFacesContext.getCurrentInstance().getPageFlowScope().get("DepartmentId");
//            JUCtrlHierNodeBinding nodeBinding = null;
//            nodeBinding = _adfTreeTableBinding.findNodeByKeyPath(getListKey(obj));
//
//            JUIteratorBinding iterator = nodeBinding.getIteratorBinding();
//            String keyStr = nodeBinding.getRowKey().toStringFormat(true);
//            iterator.setCurrentRowWithKey(keyStr);
//            RowKeySet rks = _treeTable.getSelectedRowKeys();
//            _treeTable.setDisclosedRowKeys(rks);
//        }
    }
}
