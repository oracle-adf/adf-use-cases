package com.table.view.common;

import oracle.adf.controller.faces.context.FacesPageLifecycleContext;
import oracle.adf.controller.v2.lifecycle.Lifecycle;
import oracle.adf.controller.v2.lifecycle.PagePhaseEvent;
import oracle.adf.controller.v2.lifecycle.PagePhaseListener;

public class CustomPagePhaseListener implements PagePhaseListener {
    public CustomPagePhaseListener() {
        super();
    }

    public void beforePhase(PagePhaseEvent event) {
        FacesPageLifecycleContext ctx =
            (FacesPageLifecycleContext)event.getLifecycleContext();
        if (event.getPhaseId() == Lifecycle.PREPARE_MODEL_ID) {
            onPageLoad();
        }
    }

    public void afterPhase(PagePhaseEvent event) {
    }
    // SubClasses can override this method

    public void onPageLoad() {
        //On Load logic
    }
}

