# adf-use-cases

Downloaded from:

    https://code.google.com/p/adf-use-cases/downloads/list

 ADF Coding Standards.doc

    Document on ADF Coding Standards

Tree - Tree Table Example

    Change image on expand/contract of tree table node
    https://forums.oracle.com/forums/thread.jspa?threadID=2494289&tstart=0

SelectManyShuttle - SelectManyShuttle Component implementation

    Implementing SelectManyShuttle component.

SearchForm - Cascading LOV in Query component.

    Implementing cascading LOV in query component.

MasterDetail1 - Master Detail 1

    Forum Thread: https://forums.oracle.com/forums/thread.jspa?messageID=10586226#10586226

MasterDetail - Master Detail  

    Forum thread: https://forums.oracle.com/forums/thread.jspa?messageID=10585615#10585615

LinkApp - Links in a column(table)

    Forum Thread
    https://forums.oracle.com/forums/post!reply.jspa?messageID=10581351

Navigation - Navigation based on Select One Choice value

    This is for the forum thread:
    https://forums.oracle.com/forums/thread.jspa?forumID=83&threadID=2426214

 SelectManyCheckBoxInTable - Select Many Check Box in a table

     This is similar to earlier blog on select many check box.

    There are three tables, EMP, ROLES, EMP_ROLES.
    Scenario: On a UI page, EMP information is shown in a table. Roles data is shown as SelectManyCheckBox in a column. The mapping of EMP and ROLES is stored in EMP_ROLES table.

    The user can select as many Roles as possible for an Employee record. The EMP_ROLES table will contain all the ROLES selected for an Employee.
    In EMP records, the corresponding ROLES are displayed as selected.

    http://umeshagarwal24.blogspot.in/2012/06/selectmanycheckbox-component-in-table.html


FileUploadIssue - File Upload issue.

    Use case for the issue described in the forum thread:
    https://forums.oracle.com/forums/thread.jspa?messageID=10390222#10390222


  TreeTableUseCase - Tree Table issue: Developed in jdev 11.1.2.0

    Tree Table issue: Developed in jdev 11.1.2.0


TreeTable1 - Tree Table issue: Developed in jdev 11.1.1.5

    Tree Table issue: Developed in jdev 11.1.1.5

SelectManyCheckBoxImplementation - SelectManyCheckBox Use Case

    This is for the use case described in forum thread: https://forums.oracle.com/forums/thread.jspa?forumID=83&threadID=2389694.

    There are three tables, EMP, ROLES, EMP_ROLES.
    Scenario: On a UI page, EMP table is shown in form layout. Roles table is shown as SelectManyCheckBox. The combination of EMP and ROLES is stored in EMP_ROLES table.

    The user can select as many Roles as possible for an EMployee. The EMP_ROLES table will contain all the ROLES selected for an EMployee.
    And while navigating between the records in EMP, the corresponding ROLES are displayed as selected.

    http://umeshagarwal24.blogspot.in/2012/05/adf-11g-implementation-of.html


TrainComponentUseCase - Calling a method before a train stop

    This use case is for the forum thread: https://forums.oracle.com/forums/thread.jspa?forumID=83&threadID=2389844

LOVUseCase2 - LOV Use Case: Transient attribute

    https://forums.oracle.com/forums/thread.jspa?threadID=2389734&tstart=0

 LOVUseCase - Excel import produces blank fields if used with Select One Choice

     This is for the forum thread:
     https://forums.oracle.com/forums/thread.jspa?forumID=83&threadID=2388646

fg - SelectManyList/SelectManyCheckBox Use Case

    This is for the use case described in forum thread:
    https://forums.oracle.com/forums/thread.jspa?forumID=83&threadID=2387243

TransientVO - Unable to retrieve all rows from transient view object

    Unable to retrieve all rows from transient view object(Table)

CustomDateRange - Date Compare at UI

    Date Compare at UI

CreateChildRows - Programmatically add rows to a child table

    Programmatically add rows to a child table

SelectOneChoice - Use Case of Select One Choice

    Use Case of Select One Choice

SelectManyListBox - SelectManyListBox

    This application is build for explaining the blog: http://umeshagarwal24.blogspot.in/2012/05/adf-11g-use-case-of-selectmanylistbox.html

Synerves - Use Case of SelectManyListBox
