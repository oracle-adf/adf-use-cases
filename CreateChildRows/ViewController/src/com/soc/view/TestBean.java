package com.soc.view;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;
import oracle.adf.model.binding.DCBindingContainer;

public class TestBean {
    
    private BindingContainer bindings;
    
    public void setBindings(BindingContainer bindings) {
        this.bindings = bindings;
    }

    public BindingContainer getBindings() {
        return bindings;
    }

    public String createRecord() {
        DCBindingContainer bc = (DCBindingContainer)getBindings();
        OperationBinding op1 = bc.getOperationBinding("CreateInsert");
        if(op1 != null){
            op1.execute();
        }
        OperationBinding op2 = bc.getOperationBinding("CreateInsert1");
        if(op2 != null){
            op2.execute();
            op2.execute();
        }
        return null;
    }
}
