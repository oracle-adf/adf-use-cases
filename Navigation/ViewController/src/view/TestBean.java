package view;

import oracle.adf.view.rich.context.AdfFacesContext;

public class TestBean {
    public TestBean() {
        super();
    }

    public String navigateToPages() {
        // Add event code here...
        AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
        Object page = adfFacesContext.getPageFlowScope().get("navigateTo");
        if (page != null) {
            if (page.equals("ONE"))
                return "one";
            else if (page.equals("TWO"))
                return "two";
            else
                return "three";
        }
        return null;
    }
}
