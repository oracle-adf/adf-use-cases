package com.date.view;

import java.util.Date;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;

import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputDate;
import oracle.adf.view.rich.context.AdfFacesContext;

public class TestBean {
    private RichInputDate startDateInputField;
    private RichInputDate endDateInputField;

    public TestBean() {
        super();
    }

    public void validateEndDate(FacesContext facesContext,
                                UIComponent uIComponent, Object object) {
       Date startDate = null;
       Date endDate = (Date)object;
       
       if(getStartDateInputField()!=null && getStartDateInputField().getValue()!=null)
           startDate = (Date)getStartDateInputField().getValue();
       
       if(endDate.compareTo(startDate) <= 0){
           System.out.println("throw the error: End Date is smaller than Start Date");
       }
       else
           System.out.println("no Error");
    }

    public void setStartDateInputField(RichInputDate startDateInputField) {
        this.startDateInputField = startDateInputField;
    }

    public RichInputDate getStartDateInputField() {
        return startDateInputField;
    }

    public void setEndDateInputField(RichInputDate endDateInputField) {
        this.endDateInputField = endDateInputField;
    }

    public RichInputDate getEndDateInputField() {
        return endDateInputField;
    }

//    public void onEndDateValueChange(ValueChangeEvent valueChangeEvent) {
//        Date startDate = null;
//        Date endDate = null;
//        
//        if(getStartDateInputField()!=null && getStartDateInputField().getValue()!=null)
//            startDate = (Date)getStartDateInputField().getValue();
//        
//        if(getEndDateInputField()!=null && getEndDateInputField().getValue()!=null)
//            startDate = (Date)getEndDateInputField().getValue();
//        
//        
//        
//        if(endDate.compareTo(startDate) <= 0){
//            System.out.println("throw the error: End Date is smaller than Start Date");
//        }
//    }
}
