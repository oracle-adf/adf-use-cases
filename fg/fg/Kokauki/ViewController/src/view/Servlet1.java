package view;

import java.io.*;
import java.sql.*;
import javax.servlet.*;
import javax.servlet.http.*;
public class Servlet1 extends HttpServlet {
   private static final String CONTENT_TYPE = "text/html; charset=windows-1257"; 
// Nepiecie�ams, lai lapa b?tu latvie�u valod?.

  public void doGet(HttpServletRequest req, HttpServletResponse res)
                               throws ServletException, IOException {
    Connection con = null;
    Statement stmt = null;
    ResultSet rs = null;
    res.setContentType(CONTENT_TYPE);
    PrintWriter out = res.getWriter();
     try {
      // Oracle draivera iel?d?�ana
      Class.forName("oracle.jdbc.driver.OracleDriver");
      // Savienojuma izveido�ana ar datub?zi
      con = DriverManager.getConnection(
        "jdbc:oracle:thin:@85.254.218.238:1521:rtuditf", "db_091RDB106", "db_091RDB106");
      stmt = con.createStatement();
//SQL vaic?jums rezult?ta tabulas izg?�anai
      rs = stmt.executeQuery("SELECT TERITORIJA  FROM TERITORIJAS_STATUSS");
 // HTML koda ?ener?�ana
      out.println("<HTML><HEAD><TITLE>TERITORIJAS</TITLE></HEAD>");
        out.println("<BODY>");
        out.println ("<h1><b>Teritorijas:<b></h1>");
      out.println("<UL>");
      while(rs.next()) {
// Tabulas lauku izvade
        out.println("<LI>" + rs.getString("TERITORIJA") );
      }
      out.println("</UL>");
      out.println("</BODY></HTML>");
    }
    catch(ClassNotFoundException e) {
      out.println("Couldn't load database driver: " + e.getMessage());
    }
    catch(SQLException e) {
      out.println("SQLException caught: " + e.getMessage());
    }
    finally {
    //Aizv?rt savienojumu ar datub?zi
      try {
        if (con != null) con.close();
      }
      catch (SQLException ignored) { }
    }
  }
}
