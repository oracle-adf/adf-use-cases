package view;

import java.util.ArrayList;
import java.util.Iterator;

import oracle.jbo.domain.Number;

import view.helper;

import java.util.List;

import java.util.Map;

import javax.faces.component.UISelectItems;
import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;
import oracle.adf.view.rich.component.rich.RichDocument;
import oracle.adf.view.rich.component.rich.RichForm;
import oracle.adf.view.rich.component.rich.data.RichTable;
import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.component.rich.input.RichSelectManyCheckbox;
import oracle.adf.view.rich.component.rich.input.RichSelectOneChoice;
import oracle.adf.view.rich.component.rich.layout.RichPanelGroupLayout;
import oracle.adf.view.rich.component.rich.nav.RichCommandButton;
import oracle.adf.view.rich.component.rich.output.RichMessages;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.jbo.Row;

public class koks {

    private UISelectItems si3;
    private RichSelectManyCheckbox smc1;
    private UISelectItems si1;
    private helper helper = new helper();
    private RichCommandButton cb2;

    public List getSelectedTeritory() {
        return helper.getSelectedTeritory();
    }

    public void setSelectedTeritory(List selectedList) {
        helper.setSelectedTeritory(selectedList);
    }

    public void setAllTeritory(List allTeritory) {
        helper.setAllTeritory(allTeritory);
    }

    public List getAllTeritory() {
        return helper.getAllTeritory();
    }

    public String doSubmit() {
        helper.processShuttle();
        helper.doCommit();
        return null;
    }

    public String refreshSelectedList() {
        helper.setRefreshSelectedList(true);
        return null;
    }

    public void setSi3(UISelectItems si3) {
        this.si3 = si3;
    }

    public UISelectItems getSi3() {
        return si3;
    }

    public void setSmc1(RichSelectManyCheckbox smc1) {
        this.smc1 = smc1;
    }

    public RichSelectManyCheckbox getSmc1() {
        return smc1;
    }

    public void setSi1(UISelectItems si1) {
        this.si1 = si1;
    }

    public UISelectItems getSi1() {
        return si1;
    }

    public void setCb2(RichCommandButton cb2) {
        this.cb2 = cb2;
    }

    public RichCommandButton getCb2() {
        return cb2;
    }

    public void onValueChange(ValueChangeEvent valueChangeEvent) {
        List<Number> selectedListFromUI = null;
        if (valueChangeEvent.getOldValue().equals(valueChangeEvent.getNewValue()))
            return;
        selectedListFromUI = (ArrayList<Number>)valueChangeEvent.getNewValue();
        if (selectedListFromUI != null) {
            AdfFacesContext fctx = AdfFacesContext.getCurrentInstance();
            Map<String, Object> pfs = fctx.getPageFlowScope();
            if (pfs != null) {
                pfs.put("selectedList", selectedListFromUI);
            }
        }
    }
}

