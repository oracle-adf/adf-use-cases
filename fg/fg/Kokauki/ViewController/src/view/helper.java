package view;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.util.Map;

import javax.faces.model.SelectItem;

import oracle.adf.model.BindingContext;
import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.AttributeBinding;

import oracle.binding.BindingContainer;

import oracle.binding.OperationBinding;

import oracle.jbo.Row;
import oracle.jbo.domain.Number;

public class helper {
    public helper() {

    }

    List selectedTeritory;
    List allTeritory;
    Boolean refreshSelectedList = false;

    public static List attributeListForIterator(String iteratorName,
                                                String valueAttrName) {
        BindingContext bc = BindingContext.getCurrent();
        DCBindingContainer binding =
            (DCBindingContainer)bc.getCurrentBindingsEntry();
        DCIteratorBinding iter = binding.findIteratorBinding(iteratorName);
        List attributeList = new ArrayList();
        for (Row r : iter.getAllRowsInRange()) {
            attributeList.add(r.getAttribute(valueAttrName));
        }
        return attributeList;
    }

    public static List<SelectItem> selectItemsForIterator(String iteratorName,
                                                          String valueAttrName,
                                                          String displayAttrName) {
        BindingContext bc = BindingContext.getCurrent();
        DCBindingContainer binding =
            (DCBindingContainer)bc.getCurrentBindingsEntry();
        DCIteratorBinding iter = binding.findIteratorBinding(iteratorName);
        List<SelectItem> selectItems = new ArrayList<SelectItem>();
        for (Row r : iter.getAllRowsInRange()) {
            selectItems.add(new SelectItem(r.getAttribute(valueAttrName),
                                           (String)r.getAttribute(displayAttrName)));
        }
        return selectItems;
    }

    public Number getCurrentKId() {
        BindingContext bctx = BindingContext.getCurrent();
        DCBindingContainer bindings =
            (DCBindingContainer)bctx.getCurrentBindingsEntry();
        AttributeBinding attr = (AttributeBinding)bindings.get("KlonuPk");
        Number KId = (Number)attr.getInputValue();
        return KId;
    }

    public String processShuttle() {
        BindingContext bctx = BindingContext.getCurrent();
        DCBindingContainer binding =
            (DCBindingContainer)bctx.getCurrentBindingsEntry();
        DCIteratorBinding iter =
            (DCIteratorBinding)binding.get("KloniTeritorijaView2Iterator");
        //Removing all rows for the current Kloun Pk from KloniTeritorijaView
        for (Row r : iter.getAllRowsInRange()) {
            r.remove();
        }
        int size = getSelectedTeritory().size();
        if (size > 0) {
            for (int i = 0; i < size; i++) {
                Row row = iter.getRowSetIterator().createRow();
                row.setNewRowState(Row.STATUS_INITIALIZED);
                row.setAttribute("KId", getCurrentKId());
                row.setAttribute("TId", getSelectedTeritory().get(i));
                iter.getRowSetIterator().insertRow(row);
                iter.setCurrentRowWithKey(row.getKey().toStringFormat(true));
            }
        }
        String ok = doCommit();
        return null;
    }

    public BindingContainer getBindings() {
        return BindingContext.getCurrent().getCurrentBindingsEntry();
    }

    public String doCommit() {
        BindingContainer bindings = getBindings();
        OperationBinding operationBinding =
            bindings.getOperationBinding("Commit");
        Object result = operationBinding.execute();
        if (!operationBinding.getErrors().isEmpty()) {
            return null;
        }
        return null;
    }

    public List getSelectedTeritory() {
        List selectedList = null;
        AdfFacesContext fctx = AdfFacesContext.getCurrentInstance();
        Map<String, Object> pfs = fctx.getPageFlowScope();
        if (pfs != null) {
            if (pfs.get("selectedList") == null  || refreshSelectedList)
                selectedList =
                        attributeListForIterator("KloniTeritorijaView2Iterator",
                                                 "TId");
            else
                selectedList = (List)pfs.get("selectedList");
        }
        return selectedList;
    }

    public void setSelectedTeritory(List selectedItems) {
        this.selectedTeritory = selectedItems;
    }


    public List getAllTeritory() {
        if (allTeritory == null) {
            allTeritory =
                    selectItemsForIterator("TeritorijasStatussView1Iterator",
                                           "TerId", "Teritorija");
        }
        return allTeritory;
    }

    public void setAllTeritory(List allItems) {
        this.allTeritory = allItems;
    }


    public Boolean getRefreshSelectedList() {
        return refreshSelectedList;
    }

    public void setRefreshSelectedList(Boolean refreshSelectedList) {
        this.refreshSelectedList = refreshSelectedList;
    }
}
