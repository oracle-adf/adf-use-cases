package com.lov.view;

import javax.faces.event.ActionEvent;
import javax.faces.event.ValueChangeEvent;

import oracle.adf.view.rich.component.rich.input.RichInputFile;
import oracle.adf.view.rich.component.rich.nav.RichCommandButton;

import oracle.binding.BindingContainer;

import oracle.adf.model.binding.DCBindingContainer;
import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.AttributeBinding;

import oracle.binding.OperationBinding;

import oracle.jbo.Key;
import oracle.jbo.Row;

import org.apache.myfaces.trinidad.model.UploadedFile;

public class TestBean {
    private RichCommandButton uploadButton;
    private RichInputFile uploadFile;

    public TestBean() {
        super();
    }
    private BindingContainer bindings;
    
    public void setBindings(BindingContainer bindings) {
        this.bindings = bindings;
    }

    public BindingContainer getBindings() {
        return bindings;
    }

    public String onDeleteDepartment() {
        // Add event code here...
        DCBindingContainer bc = (DCBindingContainer)getBindings();
        DCIteratorBinding ab = (DCIteratorBinding)bc.findIteratorBinding("EmployeesView1Iterator");
        Row row = ab.getCurrentRow();
        Object obj[] = {row.getAttribute("DepartmentId")};
        Key key = new Key(obj);
        OperationBinding op = (OperationBinding)bc.getOperationBinding("removeRowWithKey");
        if(op!=null){
            op.getParamsMap().put("rowKey", key.toString());
            op.execute();
        }
        return null;
    }

    public void setUploadButton(RichCommandButton uploadButton) {
        this.uploadButton = uploadButton;
    }

    public RichCommandButton getUploadButton() {
        return uploadButton;
    }

    public void setUploadFile(RichInputFile uploadFile) {
        this.uploadFile = uploadFile;
    }

    public RichInputFile getUploadFile() {
        return uploadFile;
    }

    public void onFileChange(ValueChangeEvent valueChangeEvent) {
        // Add event code here...
        if(valueChangeEvent.getNewValue() == valueChangeEvent.getOldValue())
            return;
        
        if(valueChangeEvent.getNewValue()!=null){
            getUploadButton().setDisabled(Boolean.FALSE);
            AdfFacesContext adfFacesContext = AdfFacesContext.getCurrentInstance();
            adfFacesContext.addPartialTarget(getUploadButton());
        }
    }

    public void processFile(ActionEvent actionEvent) {
        // Add event code here...
        UploadedFile file = (UploadedFile)uploadFile.getValue(); 
    }
}
