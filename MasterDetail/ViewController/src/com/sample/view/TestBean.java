package com.sample.view;

import java.util.Map;

import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.model.binding.DCIteratorBinding;

import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.adfdt.model.objects.IteratorBinding;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

import oracle.jbo.Row;

public class TestBean {
  public TestBean() {
    super();
  }
  private BindingContainer bindings;

  public void setBindings(BindingContainer bindings) {
    this.bindings = bindings;
  }

  public BindingContainer getBindings() {
    return bindings;
  }
  
  public String createDocuments(){
    DCBindingContainer dc = (DCBindingContainer)getBindings();
    OperationBinding op = dc.getOperationBinding("CreateInsert");
    Map map = (Map)AdfFacesContext.getCurrentInstance().getPageFlowScope();
    if(op!=null){
      op.execute();
    }
    DCIteratorBinding itr = dc.findIteratorBinding("DocumentsIterator");
    Row row = itr.getCurrentRow();
    row.setAttribute("ParentId", map.get("parentId"));
    return null;
  }
}
