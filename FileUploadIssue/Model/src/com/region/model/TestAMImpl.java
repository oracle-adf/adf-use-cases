package com.region.model;


import com.region.model.common.TestAM;

import java.util.HashMap;
import java.util.List;

import java.util.Map;

import oracle.jbo.Row;
import oracle.jbo.ViewCriteria;
import oracle.jbo.domain.Number;
import oracle.jbo.server.ApplicationModuleImpl;
import oracle.jbo.server.ViewObjectImpl;
// ---------------------------------------------------------------------
// ---    File generated by Oracle ADF Business Components Design Time.
// ---    Tue Jun 12 16:37:57 IST 2012
// ---    Custom code may be added to this class.
// ---    Warning: Do not modify method signatures of generated methods.
// ---------------------------------------------------------------------
public class TestAMImpl extends ApplicationModuleImpl implements TestAM {
    /**
     * This is the default constructor (do not remove).
     */
    public TestAMImpl() {
    }

    /**
     * Container's getter for EmployeesVO1.
     * @return EmployeesVO1
     */
    public ViewObjectImpl getEmployeesVO1() {
        return (ViewObjectImpl)findViewObject("EmployeesVO1");
    }
    
    public Map getEmpDetails(Number empId){
        ViewObjectImpl emp = getEmployeesVO1();
        Map empDetails = new HashMap();
        emp.applyViewCriteria(null);
        emp.setNamedWhereClauseParam("pEmpId", null);
        emp.setNamedWhereClauseParam("pEmpId", empId);
        ViewCriteria vcr = emp.getViewCriteria("EmployeesVOCriteria");
        emp.applyViewCriteria(vcr);      
        emp.executeQuery();
        emp.first();
        if(emp.getEstimatedRowCount() == 1){
            Row r = emp.getCurrentRow();
            empDetails.put("FirstName", r.getAttribute("FirstName"));
            empDetails.put("LastName",r.getAttribute("LastName"));
        }
        
        return empDetails;
    }
}
