package com.region.view;

import java.util.Map;

import javax.faces.event.ValueChangeEvent;
import oracle.jbo.domain.Number;

import oracle.adf.model.binding.DCBindingContainer;

import oracle.adf.view.rich.component.rich.input.RichInputText;
import oracle.adf.view.rich.context.AdfFacesContext;

import oracle.binding.BindingContainer;
import oracle.binding.OperationBinding;

public class TestBean {
    private RichInputText empId;

    public TestBean() {
        super();
    }
    private BindingContainer bindings;
    
    public void setBindings(BindingContainer bindings) {
        this.bindings = bindings;
    }

    public BindingContainer getBindings() {
        return bindings;
    }

    public void onEmpNumberChange(ValueChangeEvent valueChangeEvent) {
        DCBindingContainer bc = (DCBindingContainer)getBindings();
        OperationBinding op = bc.getOperationBinding("getEmpDetails");
//        Object empnumber = AdfFacesContext.getCurrentInstance().getPageFlowScope().get("empNumber");
        Object empNumber = getEmpId().getValue();
        if(op!=null){
            try{
            op.getParamsMap().put("empId", new oracle.jbo.domain.Number(empNumber));
            op.execute();
            }catch(Exception e){}
        }
        if(op.getErrors().size() == 0){
            Map det = (Map)op.getResult();
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put("firstName", det.get("FirstName"));
            AdfFacesContext.getCurrentInstance().getPageFlowScope().put("lastName", det.get("LastName"));
        }
        
    }

    public void setEmpId(RichInputText empId) {
        this.empId = empId;
    }

    public RichInputText getEmpId() {
        return empId;
    }
}
